<?php
defined('BASEPATH') OR exit('Access Denied!');

class Starter extends CI_Controller {

	public function index()
	{
			$data['page'] = 'starter';
			$data['config'] = [
				"title" => "Starter",
				"desc" => "it all starts here",
				"icon" => "dashboard"
			];

			$this->load->view('Loader', $data);
	}
}
