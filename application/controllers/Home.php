<?php
defined('BASEPATH') OR exit('Access Denied!');

require './vendor/autoload.php';
class Home extends CI_Controller {

    public function __construct()
    {
		parent::__construct();
		
		$this->load->model('auth_model');
		$this->load->model('anggota_model');
		$this->load->model('users_model');

		$this->auth_model->check();
    }

	public function index()
	{
        // Halaman Utama 
        $data['page'] = 'beranda';
        $data['name'] = $this->config->item("site_name");

        $this->load->view('Loader', $data);
    }
    
    public function anggota()
	{
        // Halaman Utama 
        $data['page'] = 'anggota';
        $data['name'] = $this->config->item("site_name");

		$data['data_anggota'] = $this->anggota_model->getAktifAll();

        $this->load->view('Loader', $data);
	}
	
    
    public function tambahAnggota()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$surat_baptis = "";
		$surat_sidi = "";
		$surat_nikah = "";

		if($_POST['status_baptis'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_baptis'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_baptis = $data['upload_data']['file_name'];
			}
		endif;

		if($_POST['status_sidi'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_sidi'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_sidi = $data['upload_data']['file_name'];
			}
		endif;

		if($_POST['status_nikah'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_nikah'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_baptis = $data['upload_data']['file_name'];
			}
		endif;
			
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["surat_baptis" => $surat_baptis, "surat_sidi" => $surat_sidi, "surat_nikah" => $surat_nikah]);
				$this->anggota_model->insert($data);
				$msg = "Data Anggota Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}
		}else {
			$msg = "Data Anggota tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
	}

    public function editAnggota()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$_data = $this->anggota_model->getByID($this->input->post('id_anggota'))[0];

		$surat_baptis = $_data->surat_baptis;
		$surat_sidi = $_data->surat_sidi;
		$surat_nikah = $_data->surat_nikah;

		if($_POST['status_baptis'] == "sudah"):
			if(!empty($_FILES['surat_baptis']['name'])):
				if ( ! $this->upload->do_upload('surat_baptis'))
				{
						$err[] = $this->upload->display_errors();
				}
				else
				{
						$data = array('upload_data' => $this->upload->data());
						$surat_baptis = $data['upload_data']['file_name'];
				}
			endif;
		endif;

		if($_POST['status_sidi'] == "sudah"):
			if(!empty($_FILES['surat_sidi']['name'])):
				if ( ! $this->upload->do_upload('surat_sidi'))
				{
						$err[] = $this->upload->display_errors();
				}
				else
				{
						$data = array('upload_data' => $this->upload->data());
						$surat_sidi = $data['upload_data']['file_name'];
				}
			endif;
		endif;

		if($_POST['status_nikah'] == "sudah"):
			if(!empty($_FILES['status_nikah']['name'])):
				if ( ! $this->upload->do_upload('surat_nikah'))
				{
						$err[] = $this->upload->display_errors();
				}
				else
				{
						$data = array('upload_data' => $this->upload->data());
						$surat_baptis = $data['upload_data']['file_name'];
				}
			endif;
		endif;
			
		$data = $this->input->post();
		if(true) {
			$id_anggota = $data['id_anggota'];
			unset($data['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["surat_baptis" => $surat_baptis, "surat_sidi" => $surat_sidi, "surat_nikah" => $surat_nikah]);
				$this->anggota_model->update($id_anggota, $data);
				$msg = "Data Anggota Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}
		}else {
			$msg = "Data Anggota tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
	}


	public function tambahMember()
    {
	
		$data = $this->input->post();
		if(true) {

			$kolom = $this->input->post('kolom');
			$nama = $this->input->post('nama');
			unset($data['kolom']);
			unset($data['nama']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["nama" => $nama, "kolom" => $kolom]);
				$this->anggota_model->insertMember($data);
				$msg = "Data Member Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/member', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/member', 'refresh');
			}
		}else {
			$msg = "Data Member tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/member', 'refresh');
		}
	}

	public function editMember()
    {
	
		$data = $this->input->post();
		if(true) {
			$id = $this->input->post('id_user');
			$kolom = $this->input->post('kolom');
			$nama = $this->input->post('nama');
			unset($data['kolom']);
			unset($data['nama']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["nama" => $nama, "kolom" => $kolom]);
				$this->anggota_model->updateMember($id, $data);
				$msg = "Data Member Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/member', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/member', 'refresh');
			}
		}else {
			$msg = "Data Member tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/member', 'refresh');
		}
	}
	
	public function pindah()
	{
        // Halaman Utama 
        $data['page'] = 'pindah';
        $data['name'] = $this->config->item("site_name");

		$data['data_anggota'] = $this->anggota_model->getPindahAll();

        $this->load->view('Loader', $data);
	}
	
	public function tambahPindah()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$dokumen_pindah = "";

		if(!empty($_FILES['dokumen_pindah']['name'])):
			if ( ! $this->upload->do_upload('dokumen_pindah'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$dokumen_pindah = $data['upload_data']['file_name'];
			}
		endif;
			
		$data = $this->input->post();
		if(true) {
			$id_anggota = $_POST['id_anggota'];
			unset($_POST['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota" => "pindah", "dokumen_pindah" => $dokumen_pindah]);
				$this->anggota_model->update($id_anggota, $data);
				$msg = "Data Pindah Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}
		}else {
			$msg = "Data Pindah tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pindah', 'refresh');
		}
	}

	public function ubahPindah()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$dokumen_pindah = "";

		if(!empty($_FILES['dokumen_pindah']['name'])):
			if ( ! $this->upload->do_upload('dokumen_pindah'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$dokumen_pindah = $data['upload_data']['file_name'];
			}
		endif;
			
		$data = $this->input->post();
		if(true) {
			$id_anggota = $_POST['id_anggota'];
			unset($_POST['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota" => "pindah", "dokumen_pindah" => $dokumen_pindah]);
				$this->anggota_model->update($id_anggota, $data);
				$msg = "Data Pindah Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}
		}else {
			$msg = "Data Pindah tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pindah', 'refresh');
		}
	}
	public function permintaanUbahPindah()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$dokumen_pindah = "";

		if(!empty($_FILES['dokumen_pindah']['name'])):
			if ( ! $this->upload->do_upload('dokumen_pindah'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$dokumen_pindah = $data['upload_data']['file_name'];
			}
		endif;
			
		$data = $this->input->post();
		if(true) {
			$id_anggota = $_POST['id_anggota'];
			unset($_POST['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota" => "pindah", "dokumen_pindah" => $dokumen_pindah]);
				$this->anggota_model->insertPermintaan($data);
				$msg = "Data Pindah Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pindah', 'refresh');
			}
		}else {
			$msg = "Data Pindah tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pindah', 'refresh');
		}
	}

	public function meninggal()
	{
        // Halaman Utama 
        $data['page'] = 'meninggal';
        $data['name'] = $this->config->item("site_name");

		$data['data_anggota'] = $this->anggota_model->getMeninggalAll();

        $this->load->view('Loader', $data);
	}
	
	public function tambahMeninggal()
    {

		$data = $this->input->post();
		if(true) {
			$id_anggota = $_POST['id_anggota'];
			unset($_POST['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota" => "meninggal"]);
				$this->anggota_model->update($id_anggota, $data);
				$msg = "Data Meninggal Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/meninggal', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/meninggal', 'refresh');
			}
		}else {
			$msg = "Data Meninggal tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/meninggal', 'refresh');
		}
	}

	public function permintaanUbahMeninggal()
    {

		$data = $this->input->post();
		if(true) {
			$id_anggota = $_POST['id_anggota'];
			unset($_POST['id_anggota']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota" => "meninggal"]);
				$this->anggota_model->insertPermintaan($data);;
				$msg = "Data Meninggal Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/meninggal', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/meninggal', 'refresh');
			}
		}else {
			$msg = "Data Meninggal tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/meninggal', 'refresh');
		}
	}

	public function member()
	{
        // Halaman Utama 
        $data['page'] = 'member';
        $data['name'] = $this->config->item("site_name");

		$data['data_member'] = $this->users_model->getAll();

        $this->load->view('Loader', $data);
	}

	public function permintaan()
	{
        // Halaman Utama 
        $data['page'] = 'permintaan';
        $data['name'] = $this->config->item("site_name");

		$data['permintaan'] = $this->anggota_model->getPermintaanPending();

        $this->load->view('Loader', $data);
	}

	public function terima_permintaan()
    {
	
			$data = $this->anggota_model->getPermintaanByID($this->input->get('id'))[0];
			if(true) {
				$id_anggota = $data->id_anggota;
				unset($data->id_anggota);
				unset($data->id_perubahan);
				unset($data->id_member);
				unset($data->status_perubahan);
				foreach($data as $item=>$value){
					if($value == ""){
						unset($data->$item);
					}
				}
	
				if(!isset($err))
				{
					//$data = array_merge($data, ["surat_baptis" => $surat_baptis, "surat_sidi" => $surat_sidi, "surat_nikah" => $surat_nikah]);
					$this->anggota_model->update($id_anggota, $data);
					$this->anggota_model->updatePermintaan($this->input->get('id'), ["status_perubahan" => "diterima"]);
					$msg = "Data Permintaan Berhasil Ditambah";
					$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
					redirect('home/permintaan', 'refresh');
				}else {
					$msg = implode(" ", $err);
					$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					redirect('home/permintaan', 'refresh');
				}
			}else {
				$msg = "Data Permintaan tidak ada";
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/permintaan', 'refresh');
			}
		}

		public function tolak_permintaan()
		{
		
				$data = $this->anggota_model->getPermintaanByID($this->input->get('id'))[0];
				if(true) {
		
					if(!isset($err))
					{
						$this->anggota_model->updatePermintaan($this->input->get('id'), ["status_perubahan" => "ditolak"]);
						$msg = "Data Permintaan Berhasil Ditolak";
						$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
						redirect('home/permintaan', 'refresh');
					}else {
						$msg = implode(" ", $err);
						$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
						redirect('home/permintaan', 'refresh');
					}
				}else {
					$msg = "Data Permintaan tidak ada";
					$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					redirect('home/permintaan', 'refresh');
				}
			}

	public function permintaanUbahAnggota()
    {
		$type_surat = "surat_" . $this->input->post('type');
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$surat_baptis = "";
		$surat_sidi = "";
		$surat_nikah = "";

		if($_POST['status_baptis'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_baptis'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_baptis = $data['upload_data']['file_name'];
			}
		endif;

		if($_POST['status_sidi'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_sidi'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_sidi = $data['upload_data']['file_name'];
			}
		endif;

		if($_POST['status_nikah'] == "sudah"):
			if ( ! $this->upload->do_upload('surat_nikah'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$surat_baptis = $data['upload_data']['file_name'];
			}
		endif;
			
		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["surat_baptis" => $surat_baptis, "surat_sidi" => $surat_sidi, "surat_nikah" => $surat_nikah]);
				$this->anggota_model->insertPermintaan($data);
				$msg = "Data Permintaan Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}
		}else {
			$msg = "Data Permintaan tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
	}

	public function rekap()
	{
        // Halaman Utama 
        $data['page'] = 'rekap';
        $data['name'] = $this->config->item("site_name");

        $this->load->view('Loader', $data);
	}

	public function cetakRekapAnggota()
    {
        $data['data_anggota'] = $this->anggota_model->getAll();
		$html = $this->load->view('pages/laporanDaftarAnggota',$data, true);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4', //F4
			'orientation' => 'L',
			'margin_left' => 2,
			'margin_right' => 2,
			'margin_top' => 5,
			'margin_bottom' => 5,
			'tempDir' => 'uploads'
			]);
		$mpdf->AddPage('P');
		$mpdf->WriteHTML($html);
		if($save)
		{
			$mpdf->Output('./report.pdf','F');
		}else {
			$mpdf->Output();
		}
	}
	
	public function cetakRekapPindah()
    {
        $data['data_anggota'] = $this->anggota_model->getPindahAll();
		$html = $this->load->view('pages/laporanDaftarPindah',$data, true);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4', //F4
			'orientation' => 'L',
			'margin_left' => 2,
			'margin_right' => 2,
			'margin_top' => 5,
			'margin_bottom' => 5,
			'tempDir' => 'uploads'
			]);
		$mpdf->AddPage('P');
		$mpdf->WriteHTML($html);
		if($save)
		{
			$mpdf->Output('./report.pdf','F');
		}else {
			$mpdf->Output();
		}
	}
	
	public function cetakRekapMeninggal()
    {
        $data['data_anggota'] = $this->anggota_model->getMeninggalAll();
		$html = $this->load->view('pages/laporanDaftarMeninggal',$data, true);

		$mpdf = new \Mpdf\Mpdf([
			'mode' => 'utf-8',
			'format' => 'A4', //F4
			'orientation' => 'L',
			'margin_left' => 2,
			'margin_right' => 2,
			'margin_top' => 5,
			'margin_bottom' => 5,
			'tempDir' => 'uploads'
			]);
		$mpdf->AddPage('P');
		$mpdf->WriteHTML($html);
		if($save)
		{
			$mpdf->Output('./report.pdf','F');
		}else {
			$mpdf->Output();
		}
    }
	
}
