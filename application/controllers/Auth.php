<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

        public function __construct()
        {
            parent::__construct();

            // auto load model
            $this->load->model('auth_model');
        }

        public function index()
        {
            // memanggil fungsi cek  login
            $this->auth_model->check();
        }

        public function login()
        {
            // Halaman Login 
            $data['page'] = 'login';
            $data['login'] = true;
            $data['name'] = $this->config->item("site_name");

            $this->load->view('Loader', $data);
        }

        public function ganti_sandi()
        {
            $this->auth_model->check();
            // Halaman Ganti Sandi
            //$data['page'] = 'ganti_sandi';
            //$data['login'] = true;
            //$data['name'] = $this->config->item("site_name");
            //$this->load->view('Loader', $data);
        }

        public function checkLogin()
        {
            // cek login
            $this->auth_model->logged_in();
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // memanggil fungsi cek login
            $this->auth_model->login($username, $password);
        }

        /*public function changePassword()
        {
            $password_new = $this->input->post('password_new');
            $password_old = $this->input->post('password_old');
            $password_confirm = $this->input->post('password_confirm');

            if ($password_new==$password_old) {
                $this->auth_model->gantiPassword($this->session->userdata("id"), $password_old, $password_new);
            }else {
                $msg = "Password tidak sama!";
                $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                redirect('auth/ganti_password', 'refresh');
            }
        }*/


        public function logout()
        {
            // FUnsi Logout
            $this->session->sess_destroy();
            redirect('auth/login');
        }
}
