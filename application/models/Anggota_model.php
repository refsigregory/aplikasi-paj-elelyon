<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("select * from tb_anggota");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getByID($id)
    {
        $query=$this->db->query("select * from tb_anggota where id_anggota = $id");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAktifAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'aktif'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPindahAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'pindah'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getMeninggalAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'meninggal'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insert($data)
    {
        $query = $this->db->insert("tb_anggota", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function update($id, $data)
    {
        $this->db->where("id_anggota", $id);
        $query = $this->db->update("tb_anggota", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function getMemberByID($id)
    {
        $query=$this->db->query("select * from tb_users where id_user = $id");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insertMember($data)
    {
        $query = $this->db->insert("tb_users", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateMember($id, $data)
    {
        $this->db->where("id_user", $id);
        $query = $this->db->update("tb_users", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function getPermintaanAll()
    {
        $query=$this->db->query("select * from tb_permintaan");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPermintaanPending()
    {
        $query=$this->db->query("select * from tb_permintaan where status_perubahan = 'menunggu'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
    

    public function getPermintaanByID($id)
    {
        $query=$this->db->query("select * from tb_permintaan where id_perubahan = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insertPermintaan($data)
    {
        $query = $this->db->insert("tb_permintaan", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updatePermintaan($id, $data)
    {
        $this->db->where("id_perubahan", $id);
        $query = $this->db->update("tb_permintaan", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function getJumlahKolom()
    {
        $query=$this->db->query("SELECT kolom, count(*) as jumlah FROM `tb_anggota` GROUP BY kolom");
        return $query->num_rows();
    }

    public function getJumlahKeluarga()
    {
        $query=$this->db->query("SELECT keluarga, count(*) as jumlah FROM `tb_anggota` GROUP BY keluarga");
        return $query->num_rows();
    }

    public function getJumlahJemaat()
    {
        $query=$this->db->query("select count(*) as jumlah from tb_anggota where status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahBapak()
    {
        $query=$this->db->query("select * from tb_anggota where jenis_kelamin = 'Laki-laki' and (year(now()) - year(tanggal_lahir)) > 25 and (year(now()) - year(tanggal_lahir)) < 60 and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahIbu()
    {
        $query=$this->db->query("select * from tb_anggota where jenis_kelamin = 'Perempuan' and (year(now()) - year(tanggal_lahir)) > 25 and (year(now()) - year(tanggal_lahir)) < 60 and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahPemuda()
    {
        $query=$this->db->query("select * from tb_anggota  where kategori_bipra = 'pemuda' and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahRemaja()
    {
        $query=$this->db->query("select * from tb_anggota where kategori_bipra = 'remaja' and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahAnak()
    {
        $query=$this->db->query("select * from tb_anggota  where kategori_bipra = 'anak' and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahLansia()
    {
        $query=$this->db->query("select * from tb_anggota where (year(now()) - year(tanggal_lahir)) >= 60 and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getJumlahLakiLaki()
    {
        $query=$this->db->query("select * from tb_anggota where jenis_kelamin = 'Laki-laki' and status_anggota = 'aktif'");
        if($query->num_rows()>0)
        return $query->num_rows();
    }

    public function getJumlahPerempuan()
    {
        $query=$this->db->query("select * from tb_anggota where jenis_kelamin = 'Perempuan' and status_anggota = 'aktif'");
        return $query->num_rows();
    }

    public function getGrafikPekerjaan()
    {
        $query=$this->db->query("select pekerjaan, count(*) as jumlah as jumlah from tb_anggota group by pekerjaan");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    

}