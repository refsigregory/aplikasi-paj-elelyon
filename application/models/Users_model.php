<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("select * from tb_users");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
}