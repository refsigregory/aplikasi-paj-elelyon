
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=base_url();?>">
        <div class="sidebar-brand-text mx-3"><?php echo $this->config->item("site_name"); ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('home')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>BERANDA</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/anggota')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>ANGGOTA JEMAAT</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/pindah')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>DATA PINDAH</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/meninggal')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>DATA KEMATIAN</span></a>
      </li>

      <?php if($this->session->userdata('type') == "admin"): ?>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/rekap')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>REKAP</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/permintaan')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>PERMINTAAN</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('home/member')?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>PELSUS</span></a>
      </li>
      <?php endif;?>
    </ul>
    <!-- End of Sidebar -->



    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$this->session->userdata("username");?> (<?=$this->session->userdata("type");?>)</span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Keluar
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
