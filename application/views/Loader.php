<?php
defined('BASEPATH') OR exit('Access Denied!');
// Default Title
if(!isset($title)) $title = $this->config->item("site_name"); 
// Show Header
if(!isset($header)) $header = true; 
// Show Navbar
if(!isset($navbar)) $navbar = true; 
// Show Content
if(!isset($content)) $content = true;
// Show Footer
if(!isset($footer)) $footer = true; 
// Is Login
if(!isset($login)) $login = false; 

if($login) {
  $header = false;
  $navbar = false;
  $content = false;
  $footer = false;
}

// Send Data
if (isset($data)) {
    foreach ($data as $key => $value) {
         $data[$key] = $data[$value]; // send $data values to page content
    }
} else {
    $data['data'] = ""; 
}

if (isset($head)) {
    foreach ($head as $key => $value) {
         $head[$key] = $dheadata[$value]; // send $data values to page content
    }
} else {
    $head['data'] = ""; 
}

if (isset($foot)) {
    foreach ($foot as $key => $value) {
         $foot[$key] = $foot[$value]; // send $data values to page content
    }
} else {
    $foot['data'] = ""; 
}

header('X-XSS-Protection: 1');
header('X-Frame-Options: SAMEORIGIN');
header('X-Content-Type-Options: nosniff');
header('Vary: Accept-Encoding');
header("Access-Control-Allow-Origin: *");
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?=$title;?></title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?=base_url();?>assets/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url()  ?>assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">


</head>

<?php if($login): ?>
<body class="bg-gradient-primary">

  <div class="container">
<?php else:?>
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">
<?php endif;?>
    <?php
        // Load Header
        if($header) $this->load->view('_templates/header.php', $head);

        // Load Navbar
        if($navbar) $this->load->view('_templates/navbar.php', $navbar);

        // Load Page
        $this->load->view('pages/'.$page);

        // Load Footer
        if($footer) $this->load->view('_templates/footer.php', $footer);
    ?>
</div>
<!-- ./wrapper -->
  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url()  ?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url()  ?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url()  ?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?= base_url()  ?>/assets/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="<?= base_url()  ?>/assets/vendor/chart.js/Chart.min.js"></script>

  <!-- Page level custom scripts -->
  <!--<script src="<?= base_url()  ?>/assets/js/demo/chart-area-demo.js"></script>
  <script src="<?= base_url()  ?>/assets/js/demo/chart-pie-demo.js"></script>-->

  <?php
  $query = $this->db->query("SELECT pekerjaan, count(*) as jumlah FROM `tb_anggota` group by pekerjaan");
?>
  <script>
  // Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: [<?php foreach ($query->result() as $d): echo '"'. $d->pekerjaan . '",'; endforeach; ?>],
    datasets: [{
      data: [<?php foreach ($query->result() as $d): echo ''. $d->jumlah . ','; endforeach; ?>],
      backgroundColor: [<?php foreach ($query->result() as $d): echo "'rgba(".rand(0,255).",".rand(0,255).",".rand(0,255).")',"; endforeach; ?>],

      //backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc'],
      //hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
      //hoverBorderColor: "rgba(234, 236, 244, 1)",
    }],
  },
  options: {
    maintainAspectRatio: false,
    tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      caretPadding: 10,
    },
    legend: {
      display: true
    },
    cutoutPercentage: 80,
  },
});

  </script>

  <script src="<?= base_url();?>/assets/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= base_url();?>/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <script>
    var _suratBaptis = $("#status_baptis");
    var _inputSuratBaptis = $("#upload_surat_baptis");
    var _suratSidi = $("#status_sidi");
    var _inputSuratSidi = $("#upload_surat_sidi");
    var _suratNikah = $("#status_nikah");
    var _inputSuratNikah = $("#upload_surat_nikah");

    _suratBaptis.change(() => {
      var pilihan = $("#status_baptis").val();
      if(pilihan == "sudah")
      {
         var form = '' +
         '<div class="form-group">' +
          '  <label>Upload Surat Baptis</label>' +
          ' <input type="file" class="form-control" name="surat_baptis" placeholder="">' +
          '</div>';
        _inputSuratBaptis.html(form);
      } else {
        _inputSuratBaptis.html('');
      }
    });

    _suratSidi.change(() => {
      var pilihan = $("#status_sidi").val();
      if(pilihan == "sudah")
      {
         var form = '' +
         '<div class="form-group">' +
          '  <label>Upload Surat Sidi</label>' +
          ' <input type="file" class="form-control" name="surat_sidi" placeholder="">' +
          '</div>';
        _inputSuratSidi.html(form);
      } else {
        _inputSuratSidi.html('');
      }
    });

    _suratNikah.change(() => {
      var pilihan = $("#status_nikah").val();
      if(pilihan == "sudah")
      {
         var form = '' +
         '<div class="form-group">' +
          '  <label>Upload Surat Nikah</label>' +
          ' <input type="file" class="form-control" name="surat_nikah" placeholder="">' +
          '</div>';
        _inputSuratNikah.html(form);
      } else {
        _inputSuratNikah.html('');
      }
    });
  </script>

  <script>
  $(function () {
    $('#show-modal').modal('show');

    $('#dataTable').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "language": {
                    "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
                    "sProcessing":   "Sedang memproses...",
                    "sLengthMenu":   "Tampilkan _MENU_ entri",
                    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Cari:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                    }
                }
    });


  function Tanya()
  {
      var tanya = confirm("Anda yakin akan melakukan aksi ini?");
      if(tanya == true)
          {
              return true;
          }else {
              return false;
          }
  }

  });
  </script>

</body>

</html>
