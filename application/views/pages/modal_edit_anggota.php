<?php
  $data = $this->anggota_model->getByID($this->input->get('edit'))[0];
?>

  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Data Anggota</h4>
      </div>
      
      <div class="modal-body">
            
            <?php if($this->session->userdata('type') == "admin"):?>
              <?=form_open_multipart(base_url('home/editAnggota'));?>
            <?php else:?>
              <?=form_open_multipart(base_url('home/permintaanUbahAnggota'));?>
              <input type="hidden" name="id_member" value="<?=$this->session->userdata('id');?>" ;?>
            <?php endif;?>
                <input type="hidden" name="id_anggota" value="<?=$data->id_anggota;?>" >

                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_lengkap" value="<?=$data->nama_lengkap;?>">
                </div>

                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin" id="">
                    <option value=""> - Pilih Jenis Kelamin - </option>
                    <option value="Laki-laki" <?=($data->jenis_kelamin == 'Laki-laki') ? 'selected="selected"' : '';?>>Laki-laki</option>
                    <option value="Perempuan" <?=($data->jenis_kelamin == 'Perempuan') ? 'selected="selected"' : '';?>>Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" name="tempat_lahir" value="<?=$data->tempat_lahir;?>">
                </div>

                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control" name="tanggal_lahir" value="<?=$data->tanggal_lahir;?>">
                </div>

                <div class="form-group">
                    <label>Kategori BIPRA</label>
                    <select class="form-control" name="kategori_bipra" id="">
                    <option value=""> - Pilih Kategori - </option>
                    <option value="bapak" <?=($data->kategori_bipra == 'bapak') ? 'selected="selected"' : '';?> <?=($data->jenis_kelamin == 'Perempuan') ? 'selected="selected"' : '';?>>Bapak</option>
                    <option value="ibu" <?=($data->kategori_bipra == 'ibu') ? 'selected="selected"' : '';?>>Ibu</option>
                    <option value="pemuda" <?=($data->kategori_bipra == 'pemuda') ? 'selected="selected"' : '';?>>Pemuda</option>
                    <option value="remaja" <?=($data->kategori_bipra == 'remja') ? 'selected="selected"' : '';?>>Remaja</option>
                    <option value="anak" <?=($data->kategori_bipra == 'anak') ? 'selected="selected"' : '';?>>Anak</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Keluarga</label>
                    <input type="text" class="form-control" name="keluarga" value="<?=$data->keluarga;?>">
                </div>

                <div class="form-group">
                    <label>Kolom</label>
                    <select class="form-control" name="kolom" id="kolom">
                    <option <?=($data->kolom == '1') ? 'selected="selected"' : '';?>>1</option>
                    <option <?=($data->kolom == '2') ? 'selected="selected"' : '';?>>2</option>
                    <option <?=($data->kolom == '3') ? 'selected="selected"' : '';?>>3</option>
                    <option <?=($data->kolom == '4') ? 'selected="selected"' : '';?>>4</option>
                    <option <?=($data->kolom == '5') ? 'selected="selected"' : '';?>>5</option>
                    <option <?=($data->kolom == '6') ? 'selected="selected"' : '';?>>6</option>
                    <option <?=($data->kolom == '7') ? 'selected="selected"' : '';?>>7</option>
                    <option <?=($data->kolom == '8') ? 'selected="selected"' : '';?>>8</option>
                    <option <?=($data->kolom == '9') ? 'selected="selected"' : '';?>>9</option>
                    </select>
                </div>


                <div class="form-group">
                    <label>Surat Baptis</label>
                    <select class="form-control" name="status_baptis" id="status_baptis">
                    <option value="belum" <?=($data->status_baptis == 'belum') ? 'selected="selected"' : '';?>>Belum</option>
                    <option value="sudah" <?=($data->status_baptis == 'sudah') ? 'selected="selected"' : '';?>>Sudah</option>
                    </select>
                </div>
                
                <div id="upload_surat_baptis"></div>

                <div class="form-group">
                    <label>Status Sidi</label>
                    <select class="form-control" name="status_sidi" id="status_sidi">
                    <option value="belum" <?=($data->status_sidi == 'belum') ? 'selected="selected"' : '';?>>Belum</option>
                    <option value="sudah" <?=($data->status_sidi == 'sudah') ? 'selected="selected"' : '';?>>Sudah</option>
                    </select>
                </div>

                <div id="upload_surat_sidi"></div>

                <div class="form-group">
                    <label>Status Nikah</label>
                    <select class="form-control" name="status_nikah" id="status_nikah">
                    <option value="belum" <?=($data->status_nikah == 'belum') ? 'selected="selected"' : '';?>>Belum</option>
                    <option value="sudah" <?=($data->status_nikah == 'sudah') ? 'selected="selected"' : '';?>>Sudah</option>
                    </select>
                </div>

                <div id="upload_surat_nikah"></div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" class="form-control" name="pekerjaan" value="<?=$data->pekerjaan;?>">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ubah Data</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->