<!-- Outer Row -->
<div class="row justify-content-center">

<div class="col-xl-6 col-lg-12 col-md-9">

  <div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
      <!-- Nested Row within Card Body -->
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">PAJ EL ELYON</h1>
            </div>
            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
            <form method="post" action="<?=base_url('auth/checkLogin');?>">
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label">Username</label>
                <div class="col-sm-8">
                <input type="text" class="form-control" name="username">
                </div>
            </div>
            <div class="form-group row">
                <label for="" class="col-sm-4 col-form-label">Password</label>
                <div class="col-sm-8">
                <input type="password" class="form-control" name="password">
                </div>
            </div>
            <div class="form-group row">
                <button type="submit" class="btn btn-primary btn-user btn-block">
                    Masuk
                </button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>