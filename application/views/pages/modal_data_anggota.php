<?php
  $data = $this->anggota_model->getByID($this->input->get('data'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DATA: <?=$data->nama_lengkap;?></h4>
      </div>
      
      <div class="modal-body">
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Nama Lengkap</label>
                <div class="col-sm-8">
                  <?=$data->nama_lengkap;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Jenis Kelamin</label>
                <div class="col-sm-8">
                  <?=$data->jenis_kelamin;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Tempat Lahir</label>
                <div class="col-sm-8">
                  <?=$data->tempat_lahir;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Tanggal Lahir</label>
                <div class="col-sm-8">
                  <?=$data->tanggal_lahir;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Kategori BIPRA</label>
                <div class="col-sm-8">
                  <?=$data->kategori_bipra;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Keluarga</label>
                <div class="col-sm-8">
                  <?=$data->keluarga;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Kolom</label>
                <div class="col-sm-8">
                  <?=$data->kolom;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Status Baptis</label>
                <div class="col-sm-8">
                  <?=$data->status_baptis;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Status Nikah</label>
                <div class="col-sm-8">
                  <?=$data->status_nikah;?>
                </div>
          </div>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Pekerjaan</label>
                <div class="col-sm-8">
                  <?=$data->pekerjaan;?>
                </div>
          </div>

          <?php if($data->dokumen_pindah != "" && $data->status_anggota == 'pindah'):?>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Berkas Pindah</label>
                <div class="col-sm-8">
                  <a href="<?=base_url('uploads/');?><?=$data->dokumen_pindah;?>" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-eye"></i>Lihat</a>
                </div>
          </div>
          <?php endif;?>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->