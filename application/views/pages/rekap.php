<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Data Anggota Jemaat</h1>
  </section>
  <?php if(!isset($_GET['tanggal'])):?>
  <section>
    <form>
      <div class="form-group">
        <input type="text" name="tanggal" value="<?=date("Y-m-d");?>">
        <button type="submit" class="btn btn-primary">Pilih Tanggal</button>
      </div>
    </form>
  </section>
<?php else:?>
  <section>
    <a href="<?=base_url('home/cetakRekapAnggota?tanggal='.$_GET['tanggal']);?>" class="btn btn-primary" target="_blank">Rekap Anggota Jemaat</a><br><br>
    <a href="<?=base_url('home/cetakRekapPindah?tanggal='.$_GET['tanggal']);?>" class="btn btn-primary" target="_blank">Rekap Data Pindah</a><br><br>
    <a href="<?=base_url('home/cetakRekapMeninggal?tanggal='.$_GET['tanggal']);?>" class="btn btn-primary" target="_blank">Rekap Data Meninggal</a><br>
  </section>
<?php endif;?>
</div>