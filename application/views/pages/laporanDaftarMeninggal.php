<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<style>
    body {
        font-family: times new roman;
    }
    .header {
        border-bottom: 4px groove black;
    }
	body {
		margin: 0;
        font-size: 10pt;
	}

    .border {
        border: groove 2px black;
        padding: 5px;
    }

	h1 {
		font-size: 10pt;
		text-align:center;
        margin:0;
        padding:0;
	}
	
    table {
		text-align:center;
		width:100%
    }

	td {
		padding: 5px;
		text-align: center;
	}
    img {
        width:100px;
    }
</style>
</head>
<body>
<div clas="header">
<table>
<tr>
<td><div style=""></div></td>
<td style="text-align:center">
<h1 style="font-size:10pt;font-weight:none;">Pendataan Anggota Jemaat</h1>
<h1 style="font-size:14pt">EL ELYON</h1>
</td>
</tr>
</table>
</div>
<div class="header"></div>
<br></br>
<?php
function dateCodeToText($date){
    $date = str_replace('Jan', 'Januari', $date);
    $date = str_replace('Feb', 'Februari', $date);
    $date = str_replace('Mar', 'Maret', $date);
    $date = str_replace('Apr', 'April', $date);
    $date = str_replace('May', 'Mei', $date);
    $date = str_replace('Jun', 'Juni', $date);
    $date = str_replace('Jul', 'Juli', $date);
    $date = str_replace('Aug', 'Agustus', $date);
    $date = str_replace('Sep', 'September', $date);
    $date = str_replace('Oct', 'Oktober', $date);
    $date = str_replace('Nov', 'November', $date);
    $date = str_replace('Dec', 'Desember', $date);
    return $date;
}
?>
<h1>Daftar Anggota Meninggal</h1>
<br>
</br>
<div>
Tanggal: <?=dateCodeToText(date("d M Y", strtotime($_GET['tanggal'])));?>
                <table class="table table-bordered" border="1" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Anggota Jemaat</th>
                      <th>Tanggal Lahir</th>
                      <th>Tanggal Meninggal</th>
                      <th>Penyebab</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($data_anggota != ""): foreach($data_anggota as $row):?>
                    <tr>
                      <td><?=$no;?></td>
                      <td><?=$row->nama_lengkap;?></td>
                      <td><?=$row->tanggal_lahir;?></td>
                      <td><?=$row->tanggal_meninggal;?></td>
                      <td><?=$row->penyebab_meninggal;?></td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
</div>
</body>
</html>