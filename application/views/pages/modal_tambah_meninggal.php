
<?php
  $data = $this->anggota_model->getByID($this->input->post('id_anggota'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Meninggal</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/tambahMeninggal'));?>
                <input type="hidden" name="id_anggota" value="<?=$data->id_anggota;?>" ;?>
                <div class="form-group">
                    <label>Anggota Jemaat</label>
                    <input type="text" disabled class="form-control" name="nama_lengkap" value="<?=$data->nama_lengkap;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Tanggal Meniggal</label>
                    <input type="text" class="form-control" name="tanggal_meninggal" value="<?=date("Y-m-d");?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Penyebab</label>
                    <input type="text" class="form-control" name="penyebab_meninggal" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->