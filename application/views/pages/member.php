<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">DATA PELSUS</h1>
  </section>
  <section>

  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
          
          <!-- DataTales -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <a href="<?=base_url('home/member/?tambah');?>"><button class="btn btn-primary float-left">TAMBAH</button></a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Lengkap</th>
                      <th>Nama Pengguna</th>
                      <th>Tipe Pengguna</th>
                      <th>Kolom</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($data_member != ""): foreach($data_member as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->nama;?></td>
                      <td><?=$row->username;?></td>
                      <td><?=str_replace("member", "pelsus", $row->type);?></td>
                      <td><?=$row->kolom;?></td>
                      <td>
                            <a href="<?=base_url('home/member?data='.$row->id_user);?>">
                              <button class="btn btn-sm btn-info" title="Lihat"><i class="fa fa-eye"></i>Lihat</button>
                            </a>
                            
                            <a href="<?=base_url('home/member?edit='.$row->id_user);?>">
                              <button class="btn btn-sm btn-warning" title="Ubah"><i class="fa fa-window-pencil"></i>Ubah</button>
                            </a>  
                      </td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
   </section>
</div>

<?php

if(isset($_GET['tambah']))
{
    $this->load->view('pages/modal_tambah_member');
}

if(isset($_GET['edit']))
{
    $this->load->view('pages/modal_edit_member');
}

if(isset($_GET['data']))
{;
    $this->load->view('pages/modal_data_member', $data);
}

?>