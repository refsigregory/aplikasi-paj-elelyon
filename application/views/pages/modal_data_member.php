<?php
  $data = $this->anggota_model->getMemberByID($this->input->get('data'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">DATA: <?=$data->nama;?></h4>
      </div>
      
      <div class="modal-body">
      <?php if($data->type == 'admin'):?>
        <div class="row">
                <label for="" class="col-sm-4 col-form-label">Username</label>
                <div class="col-sm-8">
                  <?=$data->username;?>
                </div>
          </div>
      <?php else:?>
          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Nama Lengkap</label>
                <div class="col-sm-8">
                  <?=$data->nama;?>
                </div>
          </div>

          <div class="row">
                <label for="" class="col-sm-4 col-form-label">Kolom</label>
                <div class="col-sm-8">
                  <?=$data->kolom;?>
                </div>
          </div>
      <?php endif;?>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->