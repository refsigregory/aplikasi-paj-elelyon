
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Anggota</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/tambahAnggota'));?>
                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama_lengkap" placeholder="">
                </div>

                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin" id="">
                    <option value=""> - Pilih Jenis Kelamin - </option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tempat Lahir</label>
                    <input type="text" class="form-control" name="tempat_lahir" placeholder="">
                </div>

                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control" name="tanggal_lahir" placeholder="">
                </div>

                <div class="form-group">
                    <label>Kategori BIPRA</label>
                    <select class="form-control" name="kategori_bipra" id="">
                    <option value=""> - Pilih Kategori - </option>
                    <option value="bapak">Bapak</option>
                    <option value="ibu">Ibu</option>
                    <option value="pemuda">Pemuda</option>
                    <option value="remaja">Remaja</option>
                    <option value="anak">Anak</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Keluarga</label>
                    <input type="text" class="form-control" name="keluarga" placeholder="">
                </div>

                <div class="form-group">
                    <label>Kolom</label>
                    <!--<input type="text" class="form-control" name="kolom" placeholder="">-->
                    <select class="form-control" name="kolom" id="kolom">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    </select>
                </div>


                <div class="form-group">
                    <label>Surat Baptis</label>
                    <select class="form-control" name="status_baptis" id="status_baptis">
                    <option value="belum">Belum</option>
                    <option value="sudah">Sudah</option>
                    </select>
                </div>
                
                <div id="upload_surat_baptis"></div>

                <div class="form-group">
                    <label>Status Sidi</label>
                    <select class="form-control" name="status_sidi" id="status_sidi">
                    <option value="belum">Belum</option>
                    <option value="sudah">Sudah</option>
                    </select>
                </div>

                <div id="upload_surat_sidi"></div>

                <div class="form-group">
                    <label>Status Nikah</label>
                    <select class="form-control" name="status_nikah" id="status_nikah">
                    <option value="belum">Belum</option>
                    <option value="sudah">Sudah</option>
                    </select>
                </div>

                <div id="upload_surat_nikah"></div>

                <div class="form-group">
                    <label>Pekerjaan</label>
                    <input type="text" class="form-control" name="pekerjaan" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Tambah</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->