<?php
  $data_anggota = $this->anggota_model->getAktifAll();
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Pilih Anggota</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open(base_url('home/pindah/?tambah_pindah'));?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Lengkap</th>
                      <th>Jenis Kelamin</th>
                      <th>Kategori BIRPA</th>
                      <th>Kolom</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($data_anggota != ""): foreach($data_anggota as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->nama_lengkap;?></td>
                      <td><?=$row->jenis_kelamin;?></td>
                      <td><?=$row->kategori_bipra;?></td>
                      <td><?=$row->kolom;?></td>
                      <td>
                            <input type="hidden" name="id_anggota" value="<?=$row->id_anggota;?>">
                            <button type="submit" class="btn btn-sm btn-info" title="Lihat"><i class="fa fa-check"></i>Pilih</button>
                      </td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->