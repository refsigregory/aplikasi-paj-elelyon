<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">DATA PERMINTAAN PERUBAHAN ANGGOTA JEMAAT</h1>
  </section>
  <section>

  <p class="login-box-msg">
      <?php
          if (!empty($this->session->flashdata('msg'))):
              $msg = $this->session->flashdata('msg');
      ?>
      <?php if($msg['type'] == 'success'): ?>
          <div class="alert alert-success"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'warning'): ?>
          <div class="alert alert-warning"><?=$msg['message'];?></div>
      <?php elseif ($msg['type'] == 'error'): ?>
          <div class="alert alert-danger"><?=$msg['message'];?></div>
      <?php else: ?>
          <div class="alert alert-info"><?=$msg['message'];?></div>
      <?php endif; ?>
      <?php endif; ?>
  </p>
          
          <!-- DataTales -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Lengkap</th>
                      <th>Jenis Kelamin</th>
                      <th>Kategori BIRPA</th>
                      <th>Kolom</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1; if($permintaan != ""): foreach($permintaan as $row):?>
                      <td><?=$no;?></td>
                      <td><?=$row->nama_lengkap;?></td>
                      <td><?=$row->jenis_kelamin;?></td>
                      <td><?=$row->kategori_bipra;?></td>
                      <td><?=$row->kolom;?></td>
                      <td>
                            <a href="<?=base_url('home/permintaan?data='.$row->id_anggota);?>">
                            <button class="btn btn-sm btn-info" title="Lihat"><i class="fa fa-eye"></i>Lihat</button>
                            </a>
                            
                            <a href="<?=base_url('home/terima_permintaan?id='.$row->id_perubahan);?>">
                            <button class="btn btn-sm btn-success" title="Terima"><i class="fa fa-window-check"></i>Terima</button>
                            </a>
                            
                            <a href="<?=base_url('home/tolak_permintaan?id='.$row->id_perubahan);?>">
                            <button class="btn btn-sm btn-danger" title="Terima"><i class="fa fa-window-check"></i>Tolak</button>
                            </a>
                      </td>
                    </tr>
                    <?php $no++; endforeach; endif;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
   </section>
</div>

<?php

if(isset($_GET['tambah']))
{
    $this->load->view('pages/modal_tambah_anggota', $data);
}

if(isset($_GET['data']))
{
    $this->load->view('pages/modal_data_anggota', $data);
}

if(isset($_GET['edit']))
{
    $this->load->view('pages/modal_edit_anggota', $data);
}

?>