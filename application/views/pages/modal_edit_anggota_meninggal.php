
<?php
  $data = $this->anggota_model->getByID($this->input->get('edit'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Meninggal</h4>
      </div>
      
      <div class="modal-body">
      <?php if($this->session->userdata('type') == "admin"):?>
            <?=form_open_multipart(base_url('home/ubahMeninggal'));?>
            <?php else:?>
              <?=form_open_multipart(base_url('home/permintaanUbahMeninggal'));?>
              <input type="hidden" name="id_member" value="<?=$this->session->userdata('id');?>" ;?>
            <?php endif;?>
                <input type="hidden" name="id_anggota" value="<?=$data->id_anggota;?>" ;?>
                <div class="form-group">
                    <label>Anggota Jemaat</label>
                    <input type="text" disabled class="form-control" name="nama_lengkap" value="<?=$data->nama_lengkap;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Tanggal Meniggal</label>
                    <input type="text" class="form-control" name="tanggal_meninggal" value="<?=$data->tanggal_meninggal;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Penyebab</label>
                    <input type="text" class="form-control" name="penyebab_meninggal" value="<?=$data->penyebab_meninggal;?>"  placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->