<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <section class="content-header">
    <h1 class="h3 mb-4 text-gray-800">Beranda</h1>
  </section>


  <?php if(true):?>
          <!-- Content Row -->
          <div class="row">

            <!-- Kunjungan Tamu -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Kolom</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahKolom();?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <!-- Total Antrian -->
          <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Keluarga</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahKeluarga();?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Kunjungan Tamu -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Anggota Jemaat</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahJemaat();?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <!-- Content Row -->

          <div class="row">

            <!-- Area Chart -->
            <div class="col-xl-8 col-lg-7">
              <div class="row">
                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Bapak</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahBapak();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Ibu</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahIbu();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
              </div>

              <div class="row">
                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Pemuda</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahPemuda();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Remaja</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahRemaja();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
              </div>

              <div class="row">
                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah anak</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahAnak();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Lansia</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahLansia();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
              </div>

              <div class="row">
                    <div class="col">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Laki-laki - Perempuan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?=$this->anggota_model->getJumlahLakiLaki();?> - <?=$this->anggota_model->getJumlahPerempuan();?></div>
                            </div>
                            <div class="col-auto">
                            <i class="fas fa-user fa-2x text-gray-300"></i>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
              </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Grafik Pekerjaan</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="chart-pie pt-4 pb-2">
                    <canvas id="myPieChart"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
<?php endif;?>
</div>