<?php
  $data = $this->anggota_model->getByID($this->input->get('edit'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Pindah</h4>
      </div>
      <div class="modal-body">
            <?php if($this->session->userdata('type') == "admin"):?>
              <?=form_open_multipart(base_url('home/ubahPindah'));?>
            <?php else:?>
              <?=form_open_multipart(base_url('home/permintaanUbahPindah'));?>
              <input type="hidden" name="id_member" value="<?=$this->session->userdata('id');?>" ;?>
            <?php endif;?>
                <input type="hidden" name="id_anggota" value="<?=$data->id_anggota;?>" ;?>
                <div class="form-group">
                    <label>Anggota Jemaat</label>
                    <input type="text" disabled class="form-control" name="nama_lengkap" value="<?=$data->nama_lengkap;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Jenis Pindah</label>
                    <select class="form-control" name="jenis_pindah">
                      <option <?=($data->jenis_pindah == 'Atestasi Keluar') ? 'selected="selected"' : '';?>>Atestasi Keluar</option>
                      <option <?=($data->jenis_pindah == 'Surat Pindah') ? 'selected="selected"' : '';?>>Surat Pindah</option>
                      <option <?=($data->jenis_pindah == 'Tanpa Pemberitahuan') ? 'selected="selected"' : '';?>>Tanpa Pemberitahuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tanggal Pindah</label>
                    <input type="text" class="form-control" name="tanggal_pindah" value="<?=$data->tanggal_pindah;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Dokumen Pindah</label>
                    <input type="file" class="form-control" name="dokumen_pindah" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->