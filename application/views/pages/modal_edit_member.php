<?php
    $edit = $this->anggota_model->getMemberByID($_GET['edit'])[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Member</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/editMember'));?>
                <input type="hidden" name="id_user" value="<?=$edit->id_user;?>">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username"  value="<?=$edit->username;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password"  value="<?=$edit->password;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Tipe</label>
                    <select class="form-control" name="type" id="">
                    <option <?=($edit->type == 'admin') ? 'selected="selected"' : '';?>>admin</option>
                    <option <?=($edit->type == 'member') ? 'selected="selected"' : '';?> value="member">pelsus</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Kolom</label>
                    <select class="form-control" name="kolom" id="kolom">
                    <option <?=($edit->kolom == '1') ? 'selected="selected"' : '';?>>1</option>
                    <option <?=($edit->kolom == '2') ? 'selected="selected"' : '';?>>2</option>
                    <option <?=($edit->kolom == '3') ? 'selected="selected"' : '';?>>3</option>
                    <option <?=($edit->kolom == '4') ? 'selected="selected"' : '';?>>4</option>
                    <option <?=($edit->kolom == '5') ? 'selected="selected"' : '';?>>5</option>
                    <option <?=($edit->kolom == '6') ? 'selected="selected"' : '';?>>6</option>
                    <option <?=($edit->kolom == '7') ? 'selected="selected"' : '';?>>7</option>
                    <option <?=($edit->kolom == '8') ? 'selected="selected"' : '';?>>8</option>
                    <option <?=($edit->kolom == '9') ? 'selected="selected"' : '';?>>9</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama"  value="<?=$edit->nama;?>" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
