<?php
  $data = $this->anggota_model->getByID($this->input->post('id_anggota'))[0];
?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Pindah</h4>
      </div>
      
      <div class="modal-body">
            <?=form_open_multipart(base_url('home/tambahPindah'));?>
                <input type="hidden" name="id_anggota" value="<?=$data->id_anggota;?>" ;?>
                <div class="form-group">
                    <label>Anggota Jemaat</label>
                    <input type="text" disabled class="form-control" name="nama_lengkap" value="<?=$data->nama_lengkap;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Jenis Pindah</label>
                    <select class="form-control" name="jenis_pindah">
                      <option>Atestasi Keluar</option>
                      <option>Surat Pindah</option>
                      <option>Tanpa Pemberitahuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tanggal Pindah</label>
                    <input type="text" class="form-control" name="tanggal_pindah" value="<?=date("Y-m-d");?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Dokumen Pindah</label>
                    <input type="file" class="form-control" name="dokumen_pindah" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->