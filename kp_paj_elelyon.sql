-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 01, 2019 at 08:23 PM
-- Server version: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp_paj_elelyon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id_anggota` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `jenis_kelamin` set('Laki-laki','Perempuan') NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kategori_bipra` set('anak','remaja','pemuda','bapak','ibu') NOT NULL,
  `keluarga` varchar(50) NOT NULL,
  `kolom` tinyint(4) NOT NULL,
  `status_baptis` set('sudah','belum') NOT NULL,
  `surat_baptis` varchar(100) DEFAULT NULL,
  `status_sidi` set('sudah','belum') NOT NULL,
  `surat_sidi` varchar(100) DEFAULT NULL,
  `status_nikah` set('sudah','belum') NOT NULL,
  `surat_nikah` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `status_anggota` set('aktif','pindah','meninggal') NOT NULL DEFAULT 'aktif',
  `jenis_pindah` varchar(50) DEFAULT NULL,
  `dokumen_pindah` varchar(100) DEFAULT NULL,
  `tanggal_pindah` date DEFAULT NULL,
  `tanggal_meninggal` date DEFAULT NULL,
  `penyebab_meninggal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_anggota`
--

INSERT INTO `tb_anggota` (`id_anggota`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `kategori_bipra`, `keluarga`, `kolom`, `status_baptis`, `surat_baptis`, `status_sidi`, `surat_sidi`, `status_nikah`, `surat_nikah`, `pekerjaan`, `status_anggota`, `jenis_pindah`, `dokumen_pindah`, `tanggal_pindah`, `tanggal_meninggal`, `penyebab_meninggal`) VALUES
(1, 'Alo', 'Laki-laki', 'Manado', '0000-00-00', 'bapak', 'Alo', 1, 'sudah', '92eadd05c71de2cb3dca301fd27fa3b8.jpg', 'belum', '', 'belum', '', 'Swasta', 'aktif', NULL, NULL, NULL, NULL, NULL),
(2, 'Daniel', 'Laki-laki', 'Manado', '1989-08-12', 'bapak', 'Daniel', 4, 'belum', '', 'belum', '', 'belum', '', 'Wiraswasta', 'aktif', NULL, NULL, NULL, NULL, NULL),
(3, 'Alexander', 'Laki-laki', 'Minahasa', '1951-01-08', 'bapak', 'Alexanedre', 3, 'belum', '', 'belum', '', 'belum', '', 'Pensiunan', 'meninggal', NULL, NULL, NULL, '2019-11-30', 'Sakit'),
(4, 'Altje', 'Perempuan', 'Tomohon', '1980-12-12', 'ibu', 'Alexandere', 3, 'belum', '', 'belum', '', 'belum', '', 'IRT', 'aktif', 'Tanpa Pemberitahuan', '4495385fc39833a96d6ac4beb60ef619.jpg', '2019-10-22', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_permintaan`
--

CREATE TABLE `tb_permintaan` (
  `id_perubahan` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `jenis_kelamin` set('Laki-laki','Perempuan') NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `kategori_bipra` set('anak','remaja','pemuda','bapak','ibu') NOT NULL,
  `keluarga` varchar(50) NOT NULL,
  `kolom` tinyint(4) NOT NULL,
  `status_baptis` set('sudah','belum') NOT NULL,
  `surat_baptis` varchar(100) DEFAULT NULL,
  `status_sidi` set('sudah','belum') NOT NULL,
  `surat_sidi` varchar(100) DEFAULT NULL,
  `status_nikah` set('sudah','belum') NOT NULL,
  `surat_nikah` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `status_anggota` set('aktif','pindah','meninggal') NOT NULL DEFAULT 'aktif',
  `jenis_pindah` varchar(50) DEFAULT NULL,
  `dokumen_pindah` varchar(100) DEFAULT NULL,
  `tanggal_pindah` date DEFAULT NULL,
  `tanggal_meninggal` date DEFAULT NULL,
  `penyebab_meninggal` varchar(50) DEFAULT NULL,
  `status_perubahan` set('menunggu','diterima','ditolak') NOT NULL DEFAULT 'menunggu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_permintaan`
--

INSERT INTO `tb_permintaan` (`id_perubahan`, `id_member`, `id_anggota`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `kategori_bipra`, `keluarga`, `kolom`, `status_baptis`, `surat_baptis`, `status_sidi`, `surat_sidi`, `status_nikah`, `surat_nikah`, `pekerjaan`, `status_anggota`, `jenis_pindah`, `dokumen_pindah`, `tanggal_pindah`, `tanggal_meninggal`, `penyebab_meninggal`, `status_perubahan`) VALUES
(1, 4, 4, 'Altje', 'Perempuan', 'Tomohon', '1980-12-12', 'ibu', 'Alexandere', 3, 'belum', '', 'belum', '', 'belum', '', 'IRT', 'aktif', NULL, NULL, NULL, NULL, NULL, 'diterima'),
(2, 4, 2, 'Daniel', 'Laki-laki', 'Manado', '1989-08-12', 'bapak', 'Daniel', 4, 'belum', '', 'belum', '', 'belum', '', 'Wiraswasta', 'aktif', NULL, NULL, NULL, NULL, NULL, 'ditolak'),
(3, 4, 4, '', '', '', '0000-00-00', '', '', 0, '', NULL, '', NULL, '', NULL, '', 'pindah', 'Tanpa Pemberitahuan', '', '2019-10-22', NULL, NULL, 'diterima');

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `type` set('admin','member') NOT NULL DEFAULT 'admin',
  `kolom` tinyint(4) DEFAULT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`id_user`, `username`, `password`, `type`, `kolom`, `nama`) VALUES
(1, 'admin', 'admin', 'admin', 0, 'Administrator'),
(2, 'testt', 'test', 'admin', 1, 'testt'),
(3, 'test2', 'test2', 'member', 0, '123'),
(4, 'member', 'member', 'member', 1, 'Member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `tb_permintaan`
--
ALTER TABLE `tb_permintaan`
  ADD PRIMARY KEY (`id_perubahan`),
  ADD KEY `id_anggota` (`id_anggota`) USING BTREE,
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_permintaan`
--
ALTER TABLE `tb_permintaan`
  MODIFY `id_perubahan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
